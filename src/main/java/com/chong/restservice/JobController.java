package com.chong.restservice;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

@RestController
public class JobController {

    private final AtomicLong counter = new AtomicLong();
    private final ObjectMapper objectMapper = new ObjectMapper();
    private final File json = new File("src/main/resources/jobs.json");
    private List<Job> jobList = objectMapper.readValue(json, new TypeReference<>(){});

    public JobController() throws IOException {
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PostMapping("/jobs")
    public ResponseEntity<List<Job>> addJob(@RequestBody List<Job> newJobList) throws IOException {
        jobList = newJobList;
        objectMapper.writeValue(json, jobList);
        return ResponseEntity.ok(jobList);
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @GetMapping("/jobs")
    public ResponseEntity<List<Job>> getJobs() throws IOException {
        return ResponseEntity.ok(jobList);
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PutMapping("/jobs/{id}")
    public ResponseEntity<Job> updateJob(@RequestBody Job updatedJob, @PathVariable int id) throws IOException {
        if (id <= 0 || updatedJob.getId() != id) {
            return ResponseEntity.notFound().build();
        } else {
            Job job = jobList.get(id - 1);
            job.setCompany(updatedJob.getCompany());
            job.setTitle(updatedJob.getTitle());
            job.setDateApplied(updatedJob.getDateApplied());
            objectMapper.writeValue(json, jobList);
            return ResponseEntity.ok(updatedJob);
        }
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @DeleteMapping("/jobs/{id}")
    public ResponseEntity<Job> removeJob(@PathVariable int id) throws IOException {
       if (id <= 0) {
            return ResponseEntity.notFound().build();
        } else {
            Job job = jobList.remove(id - 1);
            objectMapper.writeValue(json, jobList);
            return ResponseEntity.ok(job);
        }
    }
}
