package com.chong.restservice;

public class Job {

    private long id;
    private String company;
    private String title;
    private String dateApplied;

    public Job() {}

    public Job(long id, String company, String title, String dateApplied) {
        this.id = id;
        this.company = company;
        this.title = title;
        this.dateApplied = dateApplied;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDateApplied() {
        return dateApplied;
    }

    public void setDateApplied(String dateApplied) {
        this.dateApplied = dateApplied;
    }

    public String toString() {
        return "Job{" +
                "id=" + id +
                ", company='" + company + '\'' +
                ", title='" + title + '\'' +
                ", dateApplied='" + dateApplied + '\'' +
                '}';
    }
}
